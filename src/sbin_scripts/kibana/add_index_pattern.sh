#!/usr/bin/env bash
title="${1}"
time_field="${2}"
kibana_url="${3:-http://localhost:5601}"

if [[ ! -z "${time_field// }" ]]; then
  time_field=",\"timeFieldName\":\"${time_field//}\""
fi

curl --request POST "${kibana_url}/api/saved_objects/index-pattern" \
     --header "content-type: application/json" \
     --header "kbn-xsrf: true" \
     --data "{\"attributes\":{\"title\":\"${title}\"${time_field}}}" \
     --silent \
     --show-error
