#!/usr/bin/env bash
data_dir="${1}/cityofsacramento"

mkdir --parents "${data_dir}"

get_data() {
  local filename="${1}"
  local id="${2}"
  local type="${3:-csv}"

  curl "https://opendata.arcgis.com/datasets/${id}.${type}" \
    --output "${data_dir}/${filename}.${type}" \
    --show-error \
    --silent
}

################
# TABULAR DATA
################

# Approved Budget FY2015 - FY 2018 (http://data.cityofsacramento.org/datasets/approved-budget-fy-2015-fy-2018)
get_data "approved_budget" "00aa04d56178482b8b33248d32a2cd12_0"

# Council Office Expenses (http://data.cityofsacramento.org/datasets/council-office-expenses)
get_data "council_office_expenses" "650aabe7b2d04181afe631455b9dec64_0"

# Business Operation Tax Information (http://data.cityofsacramento.org/datasets/business-operation-tax-information)
get_data "business_operation_tax_information" "449200d5e4af4457914fdb8af0a50d30_0"

# Purchase Orders to Date (http://data.cityofsacramento.org/datasets/purchase-orders-to-date)
get_data "purchase_orders_to_date" "6339c5bb64564f05b5af764f17c07edd_0"

# Checks Issued (http://data.cityofsacramento.org/datasets/checks-issued)
get_data "checks_issued" "d9db8b64ad22495dbc13cc18279fffad_0"

# Outstanding Checks (http://data.cityofsacramento.org/datasets/outstanding-checks)
get_data "outstanding_checks" "9695f2aa9a6a4df29b732f039c77d6ab_0"

################
# SPATIAL DATA
################

# Bonded Community Facilities Districts (http://data.cityofsacramento.org/datasets/bonded-cfds)
#get_data "bonded_cfds" "c53fb70daaf24279bd0603afd7e6b3f9_0" "kml"

# Annual Service Districts (http://data.cityofsacramento.org/datasets/annual-service-districts)
#get_data "annual_service_districts" "61b5aa9de28145c1be7bfab826432401_0" "kml"

# Special Fee Districts (http://data.cityofsacramento.org/datasets/special-fee-districts)
#get_data "special_fee_districts" "9e5bebaf8c3f48a4a16bdd973e853c65_0" "kml"

# Business Improvement Districts and Business Improvement Areas (http://data.cityofsacramento.org/datasets/bids-bias)
#get_data "business_improvement_districts" "c46082e3d6824ca2a9788ea484e82e83_0" "kml"

# Bonded Assessment Districts (http://data.cityofsacramento.org/datasets/bonded-assessment-districts)
#get_data "bonded_assessment_districts" "d1aed9bb473e4652a637fd19443862e0_0" "kml"
