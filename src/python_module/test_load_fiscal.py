import unittest

from json import loads
from re import match
from mock import patch, call, ANY
from pkg_resources import resource_filename, resource_string
from tempfile import mkdtemp
from shutil import rmtree

import load_fiscal

sample_csv = resource_filename(__name__, 'sample.csv')
call_rows = [
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8880', 'General Government', 'Financial Information System', '8880.ALLO000005.20170131.0', '', '2017-01-31', '2016', '2017', '7', '5460000', 'Special Items of Expense', 'Other Special Items of Expense', 'Other Special Items of Expense', 'Taxes and Assessments', '0001', 'General Fund', 'General Fund', '6890000', 'Statewide Systems Development', 'Statewide Systems Development', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '0.0100']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8880', 'General Government', 'Financial Information System', '8880.ALLO000005.20170630.0', '', '2017-06-30', '2016', '2017', '12', '5460000', 'Special Items of Expense', 'Other Special Items of Expense', 'Other Special Items of Expense', 'Taxes and Assessments', '0001', 'General Fund', 'General Fund', '6890000', 'Statewide Systems Development', 'Statewide Systems Development', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '0.0100']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.AP00291736.20170512.0', '', '2017-05-12', '2016', '2017', '11', '5410000', 'Special Items of Expense', 'Attorneys', 'Attorneys', 'Attorney Pymts (no svcs provid', '0001', 'General Fund', 'General Fund', '6800000', 'Local Gov Audits & Review', 'Local Gov Audits & Review', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '120000.0000']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.0000312022.20170629.0', '', '2017-06-29', '2016', '2017', '12', '5399000', 'Operating Expense & Equipment', 'Special Adjustments', 'Special Adjustments', 'OE&E - Special Adjustments', '0001', 'General Fund', 'General Fund', '6770028', 'State Budget', 'Support and Direction', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '-150.0000']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.0000312022.20170629.0', '', '2017-06-29', '2016', '2017', '12', '5399000', 'Operating Expense & Equipment', 'Special Adjustments', 'Special Adjustments', 'OE&E - Special Adjustments', '0001', 'General Fund', 'General Fund', '6770028', 'State Budget', 'Support and Direction', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '150.0000']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.0000312028.20170629.0', '', '2017-06-29', '2016', '2017', '12', '5399000', 'Operating Expense & Equipment', 'Special Adjustments', 'Special Adjustments', 'OE&E - Special Adjustments', '0001', 'General Fund', 'General Fund', '6770028', 'State Budget', 'Support and Direction', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '-0.0100']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.0000312028.20170629.0', '', '2017-06-29', '2016', '2017', '12', '5399000', 'Operating Expense & Equipment', 'Special Adjustments', 'Special Adjustments', 'OE&E - Special Adjustments', '0001', 'General Fund', 'General Fund', '6770028', 'State Budget', 'Support and Direction', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '-1.0900']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.0000312028.20170629.0', '', '2017-06-29', '2016', '2017', '12', '5399000', 'Operating Expense & Equipment', 'Special Adjustments', 'Special Adjustments', 'OE&E - Special Adjustments', '0001', 'General Fund', 'General Fund', '6770028', 'State Budget', 'Support and Direction', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '-0.0900']},
    {'index': {'_type': 'budget', '_index': 'test_index'}},
    {'budget': ['8860', 'General Government', 'Department of Finance', '8860.0000312028.20170629.0', '', '2017-06-29', '2016', '2017', '12', '5399000', 'Operating Expense & Equipment', 'Special Adjustments', 'Special Adjustments', 'OE&E - Special Adjustments', '0001', 'General Fund', 'General Fund', '6770028', 'State Budget', 'Support and Direction', '001', 'State Operations', 'Budget Act', 'BA State Operations-Support001', '2016', '-1.1600']}
]
mock_api_v1_report = loads(resource_string(__name__, 'mock_api_v1_report.json'))
mock_transaction_v2_schema = loads(resource_string(__name__, 'mock_transaction_v2_schema.json'))
mock_download_url = 'https://s3.amazonaws.com/og-datamanager-uploads/production/' \
                    'grid_data_api/dataset_exports/3cb34030-9881-4a4c-b6ac-3c2da9a1d9a9/csvs/original/' \
                    'fiscalca20181105-26483-1v36dkc_all.csv?1541392146'

class MockHead:
    def __init__(self, status_code):
        self.status_code = status_code


class TestLoadFiscal(unittest.TestCase):
    @patch('os.path.exists', return_value=True)
    def test_parse_args_default(self, _):
        with patch('sys.argv', ['Hi mom!']):
            args = load_fiscal.parse_args()
            self.assertEqual(args.directory, '/var/lib/fiscal')

    @patch('os.path.exists', return_value=True)
    def test_parse_args(self, _):
        with patch('sys.argv', ['Hi mom!', '--directory', '/foo/bar']):
            args = load_fiscal.parse_args()
            self.assertEqual(args.directory, '/foo/bar')

    # Directory does not exist
    def test_get_file_url(self):
        def mocked_requests_get(*args):
            class MockResponse:
                def __init__(self, data):
                    self.data = data

                def json(self):
                    return self.data
            # 0b44fa0c-5215-4e27-bf6d-4034ae5c035d
            url = args[0]
            response = None
            if match(r'^https://fiscalca.opengov.com/api/v1/reports/\d{5}$', url):
                response = MockResponse(mock_api_v1_report)
            elif match(
                r'^https://fiscalca.opengov.com/api/transactions/v2/schema/[a-f\d]{8}-([a-f\d]{4}-){3}[a-f\d]{12}$',
                url
            ):
                response = MockResponse(mock_transaction_v2_schema)
            return response
        with patch('requests.get', side_effect=mocked_requests_get):
            self.assertEqual(load_fiscal.get_file_url(12345), mock_download_url)

    @patch('os.path.exists', return_value=True)
    @patch.object(load_fiscal, 'get_report')
    def test_main(self, mock_get_report, _):
        url = 'http://localhost:9200'
        load_fiscal.main(url)
        self.assertEqual(mock_get_report.call_count, 2)
        mock_get_report.assert_has_calls([
            call({'id': 26101, 'type': 'vendor'}, url),
            call({'id': 26103, 'type': 'spending'}, url)
        ])

    def test_main_invalid(self):
        with self.assertRaisesRegexp(IOError, 'Directory does not exist$'):
            load_fiscal.main('hello world')

    @patch.object(load_fiscal, 'get_file_url', return_value='http://localhost/gfu_return')
    @patch.object(load_fiscal, 'download_csv')
    @patch('sys.stdout')
    def test_get_report(self, _, mock_dc, mock_gfu):
        report_id = {'id': 26101, 'type': 'vendor'}
        url = 'http://localhost:9200'
        load_fiscal.get_report(report_id, url)
        mock_gfu.assert_called_once_with(report_id['id'])
        mock_dc.assert_called_once_with(ANY, 'http://localhost/gfu_return')
