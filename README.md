# Fiscal ELK

[![pipeline status](https://gitlab.com/smythian/fiscal-elk/badges/master/pipeline.svg)](https://gitlab.com/smythian/fiscal-elk/commits/master)
[![coverage report](https://gitlab.com/smythian/fiscal-elk/badges/master/coverage.svg)](https://gitlab.com/smythian/fiscal-elk/commits/master)
[![License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/smythian/fiscal-elk/blob/master/LICENSE)
[![Maintenance](https://img.shields.io/badge/maintained-Yes-brightgreen.svg)](https://gitlab.com/smythian/fiscal-elk/activity)

Fiscal ELK is a Kibana frontend to [California Open FI$Cal](http://fiscalca.opengov.com) and [City of Sacramento](http://data.cityofsacramento.org/) budget data. Server built with Terraform. Data ingested via Logstash and stored in Elasticsearch.

![FI$Cal dashboard screenshot](dashboard.png "FI$Cal dashboard screenshot")

## Getting Started

The site is deployed to <https://fiscal.smyth.app>

### Data

| Source | Dataset | Update Frequency | URL  |
| ------ |-------- | ---------------- | ---- |
| California Open FI$Cal | Spending Transactions | Daily | https://fiscalca.opengov.com/data/#/26103 | 
| California Open FI$Cal | Vendor Transactions | Daily | https://fiscalca.opengov.com/data/#/26101 | 
| Sacramento Open Data | Approved Budget FY2015 - FY 2018 | Daily | http://data.cityofsacramento.org/datasets/approved-budget-fy-2015-fy-2018 | 
| Sacramento Open Data | Council Office Expenses | Daily | http://data.cityofsacramento.org/datasets/council-office-expenses | 
| Sacramento Open Data | Business Operation Tax Information | Daily | http://data.cityofsacramento.org/datasets/business-operation-tax-information | 
| Sacramento Open Data | Purchase Orders to Date | Daily | http://data.cityofsacramento.org/datasets/purchase-orders-to-date | 
| Sacramento Open Data | Checks Issued | Daily | http://data.cityofsacramento.org/datasets/checks-issued | 
| Sacramento Open Data | Outstanding Checks | Daily | http://data.cityofsacramento.org/datasets/outstanding-checks | 

### Prerequisites

Install prerequisites then run `setup.sh` to install dependencies and setup git hooks 

* [Terraform](https://www.terraform.io/intro/getting-started/install.html)
* [Python 2.7](https://www.python.org/download/releases/2.7/)
* [pip](https://pip.pypa.io/en/stable/installing/)
* [pipenv](https://pypi.org/project/pipenv/)

## Deployment
This project is configured to use the Gitlab CI/CD pipeline to deploy (~15 minutes to deploy). Provisioning is handled via Terraform.

The following CI variables must be configured:

- `DIGITAL_OCEAN_API_KEY` - A Digital Ocean [API token](https://cloud.digitalocean.com/account/api/tokens).
- `DIGITAL_OCEAN_SSH_PRIVATE_KEY` and `DIGITAL_OCEAN_CI_SSH_FINGERPRINT` - The [SSH key](https://cloud.digitalocean.com/account/security#keys) for the CI pipeline.
- `DIGITAL_OCEAN_ELK_FLOATING_IP` - The floating IP address for the droplet that DNS records will point to.
- `DIGITAL_OCEAN_ELK_FQDN` - The server's fully qualified domain name.
- `LETS_ENCRYPT_EMAIL` - The email to use for Let's Encrypt registration. If not provided, the server will use a self-signed certificate, and nobody wants that.

After deploying, log into the server via ssh to add accounts for users to modify the Kibana frontend. Running the following script will create a [Diceware](http://diceware.com) style password for the specified user (or the current user if no username is provided) and add or update that user's entry in nginx's htpassword file.

```bash
~/set_nginx_pass.sh [username]
```

WARNING For v1, any user that can access the Kibana frontend has administrative control.

## Built With

- Terraform
- Elasticsearch
- Logstash
- Kibana
- Nginx
- Python 2.7

## Contributing
We haven't opened this project for external contributors, but plan to do so in the future.

## Security
This system is partially hardened to the level 2 profile of the
[Center for Internet Security](https://www.cisecurity.org/) Ubuntu Linux 18.04 LTS benchmark version 1.0.0. Files in
this project may include comments along the lines of `# CIS_Ubuntu_18_04_v1_0_0` to indicate the CIS recommendation
the configuration item corresponds to.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/smythian/fiscal-elk/tags). 

## Authors

* [Eric Smyth](https://gitlab.com/smythian)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
