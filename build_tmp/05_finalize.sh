#!/usr/bin/env bash

echo "[FINALIZE] Remove build files" \
  && echo "[FINALIZE] Remove root ssh key" \
  && rm -rf /root/.ssh \
  && echo "[FINALIZE] Reboot" \
  && rm -rf /root/build \
  && (sleep 2; reboot)&
