#!/usr/bin/env bash


script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source "${script_dir}/apt_helpers.sh"

echo "[INSTALL] Stopping apt-daily, cron, and at daemons that may be responsible for intermittent apt failures"
systemctl stop apt-daily-upgrade.timer
systemctl stop apt-daily.timer
systemctl stop apt-daily.service
systemctl stop atd.service
systemctl stop cron.service


echo "[INSTALL] Configure DNS"
cat >> /etc/netplan/02-fiscal.yaml << \EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      nameservers:
        addresses: [1.1.1.1, 9.9.9.9, 208.67.222.222, 8.8.8.8]

EOF
netplan apply

# CIS_Ubuntu_18_04_v1_0_0 2.3.4 (remove telnet and rsyslog)
apt_get update \
  && apt_get --yes --purge autoremove \
  && apt_get --yes purge telnet rsyslog \
  && apt_get clean \
  && echo "[INSTALL] Adding elastic source" \
  && add_key "https://artifacts.elastic.co/GPG-KEY-elasticsearch" \
  && echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" >> /etc/apt/sources.list.d/elastic-6.x.list \
  && echo "[INSTALL] Adding nginx source" \
  && add_key "https://nginx.org/keys/nginx_signing.key" \
  && echo "deb http://nginx.org/packages/mainline/ubuntu/ bionic nginx" >> /etc/apt/sources.list.d/nginx-bionic.list \
  && echo "deb-src http://nginx.org/packages/mainline/ubuntu/ bionic nginx" >> /etc/apt/sources.list.d/nginx-bionic.list \
  && echo "[INSTALL] Adding certbot source" \
  && add-apt-repository --yes ppa:certbot/certbot > /dev/null \
  && apt_get update \
  && apt_get --yes install apt-transport-https \
  && apt_get --yes upgrade \
  && apt_get --yes install --no-install-recommends \
    auditd \
    cracklib-runtime\
    diceware \
    do-agent \
    haveged \
    libpam-google-authenticator \
    libpam-pwquality \
    libssl-dev \
    logrotate \
    nginx \
    openjdk-8-jre \
    openntpd \
    python \
    python-certbot-nginx \
    python-pip \
    python-setuptools \
    python-toml \
    rng-tools \
    syslog-ng \
    tcpd \
    ufw \
    unattended-upgrades \
    unzip \
  && apt_get --yes install \
    elasticsearch \
    kibana \
    logstash

# Just systemd things
systemctl --quiet daemon-reload

echo "[INSTALL] Disable services"
systemctl --quiet disable atd rsync

systemctl start syslog-ng # todo
echo "[INSTALL] Enable services"
systemctl --quiet enable \
  auditd \
  elasticsearch \
  kibana \
  logstash \
  nginx \
  syslog-ng

echo "[INSTALL] Install logstash plugins"
lsp="/usr/share/logstash/bin/logstash-plugin"
"${lsp}" install logstash-filter-translate && \
  "${lsp}" install logstash-filter-tld && \
  "${lsp}" install logstash-filter-rest && \
  "${lsp}" install logstash-filter-elasticsearch

echo "[INSTALL] Install python packages"
packages=`echo -e "import toml\nprint(u' '.join(toml.load('/root/build/Pipfile')['packages'].keys()))" | python`
pip install -qqq ${packages}

echo "[INSTALL] Install ModSecurity for nginx"
"${script_dir}/02a_modsecurity_for_nginx.sh"
