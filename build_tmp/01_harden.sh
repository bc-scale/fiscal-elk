#!/usr/bin/env bash

mkdir --parents /root/images/

function build_partition_file() {
  local partition="${1}"
  local size="${2}"
  local filename="/root/images/${partition//\//.}.bin"
  local tmp_partition="/root/build/partitions${partition}"

  echo "[HARDEN] Building '${partition}' partition"
  # Ensure partition exists
  mkdir --parents "${partition}" > /dev/null \
    `# Create a temporary mount point for copying files over` \
    && mkdir --parents --mode=1700 "${tmp_partition}" > /dev/null \
    `# Create the file to hold the partition` \
    && dd if=/dev/zero of="${filename}" bs=1 count=0 seek="${size}" > /dev/null \
    `# Create the ext4 file system in the partition file` \
    && mkfs.ext4 "${filename}" > /dev/null \
    `# Mount the partition file to the temporary mount point` \
    && mount --options loop,rw,nodev,nosuid,noexec "${filename}" "${tmp_partition}" > /dev/null \
    `# Move the contents of the old directory to the new` \
    && rsync -aqxP "${partition}/" "${tmp_partition}/" > /dev/null \
    `# Unmount the partition file from the temporary mount point` \
    && umount "${tmp_partition}" > /dev/null \
    `# Create the fstab entry for the partition` \
    && echo "LABEL=var-log-audit ${filename} ${partition} ext4 loop,rw,noexec,nosuid,nodev 0 0" | \
      tee --append /etc/fstab > /dev/null \
    `# Mount the partition` \
    && mount "${filename}" "${partition}" > /dev/null \
    `# Remove the temporary partition` \
    && rm -rf "${tmp_partition}" > /dev/null

  return $?
}

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 1.1
###########################################

# TODO Investigate feasibility
# 1.1.5
#build_partition_file /var "10G" || exit $?

# 1.1.6, 1.1.7, 1.1.8, 1.1.9
#build_partition_file /var/tmp "1G" || exit $?

# 1.1.10
#build_partition_file /var/log "5G" || exit $?

# 1.1.11
#build_partition_file /var/log/audit "1G" || exit $?

# 1.1.2, 1.1.3, 1.1.4
#build_partition_file /tmp "1G" || exit $?

# 1.1.12, 1.1.13
#build_partition_file /home "1G" || exit $?

# 1.1.14, 1.1.15, 1.1.16
#echo "tmpfs   /dev/shm   tmpfs    rw,noexec,nosuid,nodev,bind    0 0" >> /etc/fstab

# 1.1.20
echo "[HARDEN] Ensure sticky bit is set on all world-writable directories"
df --local --portability | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d -perm -0002 2>/dev/null | xargs chmod a+t > /dev/null

# 1.1.21
if service --status-all | grep -Fq autofs && systemctl is-enabled autofs; then
  echo "[HARDEN] Disable autofs"
  systemctl --quiet disable autofs
fi

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 1.3
###########################################
# TODO Install and configure AIDE CIS_Ubuntu_18_04_v1_0_0 1.3

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 1.4
###########################################
# 1.4.1
echo "[HARDEN] Ensure permissions on bootloader config are configured"
chown --quiet root:root /boot/grub/grub.cfg
chmod --quiet og-rwx /boot/grub/grub.cfg

# 1.4.2
# TODO Investigate feasibility

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 3.4
###########################################
# TODO Investigate feasibility
