#!/usr/bin/env bash

do_token="${1}"
domain_name="${2}"
lets_encrypt_email="${3}"
email_re="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"

###########################################
# Add users
###########################################
echo "[CONFIGURE] Configure users"
touch /etc/libuser.conf > /dev/null || exit $?

addgroup fiscal > /dev/null || exit $?

adduser --system ssh_updater > /dev/null || exit $?
adduser --system --group --no-create-home fiscal_updater > /dev/null || exit $?
adduser --system --group --no-create-home logger > /dev/null || exit $?
adduser --system --group --no-create-home logrotate > /dev/null || exit $?

usermod --append --groups fiscal_updater logstash

echo "[CONFIGURE] Set permissions and ownership for user-related scripts"
find /usr/bin/fiscal_elk/ -name "*.sh" -exec chmod 700 {} \;
chown -R fiscal_updater:fiscal_updater /usr/bin/fiscal_elk/

find /usr/sbin/fiscal_elk/ -name "*.sh" -exec chmod 700 {} \;
chmod +x /etc/skel/set_nginx_pass.sh

echo "[CONFIGURE] Reload syslog-ng"
syslog-ng-ctl reload

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 1.7
###########################################
echo "[CONFIGURE] Secure motd, issue, issue.net"
# 1.7.1.4
chown root:root /etc/motd
chmod 644 /etc/motd

# 1.7.1.5
chown root:root /etc/issue
chmod 644 /etc/issue

# 1.7.1.6
chown root:root /etc/issue.net
chmod 644 /etc/issue.net

###########################################
# Configure host firewall
###########################################
echo "[CONFIGURE] Update firewall"
ufw default deny outgoing > /dev/null || exit $?
ufw default deny incoming > /dev/null || exit $?
ufw default deny forward > /dev/null || exit $?

iptables -A OUTPUT -p tcp -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p udp -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED -j ACCEPT

ufw allow from 127.0.0.0/8 to 127.0.0.1/8 || exit $?
ufw allow in 'OpenSSH' > /dev/null || exit $?
ufw allow 80 > /dev/null || exit $?
ufw allow 443 > /dev/null || exit $?
ufw allow in 5044 > /dev/null || exit $?
ufw allow out 53 > /dev/null || exit $?
ufw allow out 80 > /dev/null || exit $?
ufw allow out 443 > /dev/null || exit $?
ufw allow out 68/udp > /dev/null || exit $?
ufw allow out 123/udp > /dev/null || exit $?
ufw logging on > /dev/null || exit $?
ufw --force enable > /dev/null || exit $?

# CIS_Ubuntu_18_04_v1_0_0 4.1.3
echo "[CONFIGURE] GRUB"
echo -e "\nGRUB_CMDLINE_LINUX=\"audit=1\"" >> /etc/default/grub
update-grub

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 5.1
###########################################
echo "[CONFIGURE] Secure cron"
# 5.1.2
chown root:root /etc/crontab
chmod og-rwx /etc/crontab

# 5.1.3
chown root:root /etc/cron.hourly
chmod og-rwx /etc/cron.hourly

# 5.1.4
chown root:root /etc/cron.daily
chmod og-rwx /etc/cron.daily

# 5.1.5
chown root:root /etc/cron.weekly
chmod og-rwx /etc/cron.weekly

# 5.1.6
chown root:root /etc/cron.monthly
chmod og-rwx /etc/cron.monthly

# 5.1.7
chown root:root /etc/cron.d
chmod og-rwx /etc/cron.d

# 5.1.8
if [[ -f /etc/cron.deny ]]; then rm -f /etc/cron.deny; fi
if [[ -f /etc/cron.deny ]]; then rm -f /etc/at.deny; fi
touch /etc/cron.allow
touch /etc/at.allow
chmod og-rwx /etc/cron.allow
chmod og-rwx /etc/at.allow
chown root:root /etc/cron.allow
chown root:root /etc/at.allow

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 5.2
###########################################
echo "[CONFIGURE] Secure sshd_config"
# 5.2.1
chown root:root /etc/ssh/sshd_config
chmod og-rwx /etc/ssh/sshd_config

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 5.3
###########################################
echo "[CONFIGURE] Configure PAM"
# 5.3.1
echo -e "\npassword requisite pam_pwquality.so retry=3" >> /etc/pam.d/common-password

# 5.3.2
echo -e "\nauth required pam_tally2.so onerr=fail audit silent deny=5 unlock_time=900" >> /etc/pam.d/common-auth

# 5.3.3
echo -e "\npassword required pam_pwhistory.so remember=5" >> /etc/pam.d/common-password

# 5.3.4
#echo -e "\npassword [success=1 default=ignore] pam_unix.so sha512" >> /etc/pam.d/common-password

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 5.4
###########################################
# 5.4.1.1
sed -i -- 's/^PASS_MAX_DAYS\s*\d*$/PASS_MAX_DAYS   365/g' /etc/login.defs

# 5.4.1.2
sed -i -- 's/^PASS_MIN_DAYS\s*\d*$/PASS_MIN_DAYS   7/g' /etc/login.defs

# 5.4.1.3
sed -i -- 's/^PASS_WARN_AGE\s*\d*$/PASS_WARN_AGE   14/g' /etc/login.defs

# 5.4.1.4 Ensure inactive password lock is 30 days
useradd -D -f 30

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 5.6
###########################################
# 5.6
sed -i -- 's/^# auth       required   pam_wheel.so$/auth       required   pam_wheel.so/g' /etc/pam.d/su

###########################################
# CIS_Ubuntu_18_04_v1_0_0 section 6.1
###########################################
echo "[CONFIGURE] Secure local authentication related files"
# 6.1.2
chown root:root /etc/passwd
chmod 644 /etc/passwd

# 6.1.3
chown root:shadow /etc/shadow
chmod o-rwx,g-wx /etc/shadow

# 6.1.4
chown root:root /etc/group
chmod 644 /etc/group

# 6.1.5
chown root:shadow /etc/gshadow
chmod o-rwx,g-rw /etc/gshadow

# 6.1.6
chown root:root /etc/passwd-
chmod u-x,go-wx /etc/passwd-

# 6.1.7
chown root:shadow /etc/shadow-
chmod o-rwx,g-rw /etc/shadow-

# 6.1.8
chown root:root /etc/group-
chmod u-x,go-wx /etc/group-

# 6.1.9
chown root:shadow /etc/gshadow-
chmod o-rwx,g-rw /etc/gshadow-

# CIS_Ubuntu_18_04_v1_0_0 1.4.3
echo "[CONFIGURE] Changing the root password to a random 100 character string"
echo "root:$(< /dev/urandom tr -dc A-Z-a-z0-9 | head -c100; echo;)" | chpasswd

echo "[CONFIGURE] Set permissions for Digital Ocean API token"
do_file="/home/ssh_updater/do_token"
echo "${do_token}" > "${do_file}"
chown ssh_updater:nogroup "${do_file}"
chmod 400 "${do_file}"

echo "[CONFIGURE] Set the domain name"
sed -i "s/DIGITAL_OCEAN_ELK_FQDN/${domain_name}/g" /etc/openssl/openssl.cnf
sed -i "s/DIGITAL_OCEAN_ELK_FQDN/${domain_name}/g" /etc/nginx/nginx.conf

echo "[CONFIGURE] Add users"
sudo --user=ssh_updater sudo /usr/sbin/fiscal_elk/update_users.sh

if [[ "${lets_encrypt_email}" =~ ${email_re} ]]; then
  echo "[CONFIGURE] Configuring Let's Encrypt"
  sed --in-place "s/# LETS_ENCRYPT_ONLY//g" /etc/nginx/nginx.conf
  certbot -n --nginx -d "${domain_name}" --agree-tos -m "${lets_encrypt_email}"
else
  key_file="/etc/ssl/private/nginx.key"
  crt_file="/etc/ssl/certs/nginx.crt"
  openssl req \
    -x509 \
    -nodes \
    -days 365 \
    -subj "/C=US/ST=CA/L=Sacramento/O=Code for Sacramento/OU=FI\$Cal/CN=${domain_name}" \
    -new \
    -keyout "${key_file}" \
    -out "${crt_file}"
fi

echo "[CONFIGURE] Generate Diffie-Helman parameters"
openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096

echo "[CONFIGURE] Fiscal data directory"
fiscal_dir="/var/lib/fiscal"
mkdir -p "${fiscal_dir}"
chown fiscal_updater:fiscal_updater "${fiscal_dir}"
chmod 770 "${fiscal_dir}"
