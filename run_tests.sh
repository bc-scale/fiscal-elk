#!/usr/bin/env sh

echo "Testing python"
pipenv run coverage run --source=src \
  --module unittest discover --start-directory=src \
  && pipenv run coverage report
